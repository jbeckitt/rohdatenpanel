# RohdatenPanel
Das RohdatenPanel ermöglicht es, Daten aus Excel-Dateien in das von Raschendler verwendete TSV-Format zu übertragen.

Um dieses Panel zu verwenden, öffnen Sie einfach die Excel-Datei. Dann können Sie einen Block von Zellen auswählen, um sie mit der Schaltfläche "Daten übertragen" entweder in das Eingabe- oder in das Ausgabe-Panel zu verschieben. Danach können Sie die Daten bearbeiten, indem Sie Zeilen ziehen oder darauf klicken, und Sie können Berechnungen wie Minimum, Maximum und Durchschnittswerte für jede Spalte sehen. Wenn Sie fertig sind, klicken Sie einfach auf "Datensatz speichern", um die Daten als TSV-Datei zu speichern.

## Bibliotheken
* Apache POI wird benutzt, um Excel-Dateien zu lesen.
* Icons sind aus der Breeze-Iconsatz von KDE: unter der [LGPL-2.1-Lizenz](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html).