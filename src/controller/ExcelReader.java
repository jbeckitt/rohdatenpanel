package controller;

import java.io.File;
import java.io.IOException;

import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * Class, die Excel-Dateien liest.
 */
public final class ExcelReader {
    /**
     * Stream ƒür der XLSX/XLS-Datei.
     */
    private final File stream;
    /**
     * Der Excel-Workbook.
     */
    private final Workbook wb;
    /**
     * Der Name der Datei.
     */
    private String fileName;

    /**
     * @return Der aktuellen Stream für der Datei.
     */
    public File getStream() {
        return this.stream;
    }

    /**
     * @return Der aktuellen Workbook.
     */
    public Workbook getWb() {
        return this.wb;
    }

    /**
     * @param index Index für der Sheet.
     * @return Der Sheet an der gewählten Index.
     */
    public Sheet getSheet(final int index) {
        return this.wb.getSheetAt(index);
    }

    /**
     * @return Der Aktuellen Anzahl der Sheets.
     */
    public int getSheetCount() {
        return this.wb.getNumberOfSheets();
    }

    /**
     * @param index Der Index für der Sheet-Name.
     * @return Der Name der Sheet an der Index.
     */
    public String getSheetName(final int index) {
        return this.wb.getSheetAt(index).getSheetName();
    }

    /**
     * @return Der aktuellen FormulaEvaluator für der Workbook.
     */
    public FormulaEvaluator getEvaluator() {
        return this.wb.getCreationHelper().createFormulaEvaluator();
    }

    /**
     * @return Der Filename für der Workbook.
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * @param fm Der neue FileName.
     */
    public void setFileName(final String fm) {
        this.fileName = fm;
    }

    /**
     * @param fn FileName für der Workbook.
     * @throws IOException Als der Datei nicht zugreifbar ist.
     */
    public ExcelReader(final String fn) throws IOException {
        super();
        this.fileName = fn;
        this.stream = new File(this.fileName);
        this.wb = WorkbookFactory.create(this.stream);
    }
}
