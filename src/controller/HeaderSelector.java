// HeaderSelector.java
package controller;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import model.DatenverarbeitungTableModel;
import view.DatenverarbeitungTabellenPanel;
import view.HeaderEditor;

/**
 * Erlaubt es, auf die Überschriften zu klicken und die Spalten zwischen Ein-
 * und Ausgabe umzuschalten.
 */
public final class HeaderSelector extends MouseAdapter {
    /**
     * Der Editor, die erlaubt man, der Spalten zu wechseln.
     */
    private final HeaderEditor editor;

    /**
     * @param e Der Mouse-Event, um zu prozessieren.
     */
    public void mouseClicked(final MouseEvent e) {
        Object source = e.getSource();
        JTableHeader th = (JTableHeader) source;
        Point p = e.getPoint();
        int col = this.getColumn(th, p);
        System.out.println(col);
        TableColumn column = th.getColumnModel().getColumn(col);
        Object hv = column.getHeaderValue();
        String oldValue = (String) hv;
        this.editor.showEditor(th, col, oldValue);
        th.resizeAndRepaint();
    }

    /**
     * @param e Der Mouse-Event, um zu bearbeiten.
     */
    public void mouseDragged(final MouseEvent e) {
        super.mouseDragged(e);
    }

    /**
     * @param th Der Table-Header für der Spalten.
     * @param p Der Punkt, wo der Maus geklickt wird.
     * @return Der Spalte, wo der Maus geklickt wird.
     */
    private int getColumn(final JTableHeader th, final Point p) {
        TableColumnModel model = th.getColumnModel();
        int col = 0;

        for (int colCount = model.getColumnCount(); col < colCount; ++col) {
            if (th.getHeaderRect(col).contains(p)) {
                return col;
            }
        }

        return -1;
    }

    /**
     * @param it Input-Table für der Header-Selector
     * @param itm Das Modell für der Input-Table.
     * @param ot Output-Table für der Header-Selector
     * @param otm Das Modell für der Output-Table.
     * @param p Der Panel, wo die Tabellen stehen.
     */
    public HeaderSelector(final JTable it,
                          final DatenverarbeitungTableModel itm,
                          final JTable ot,
                          final DatenverarbeitungTableModel otm,
                          final DatenverarbeitungTabellenPanel p) {
        super();
        this.editor = new HeaderEditor(it,
                itm, ot, otm,
                p);
    }
}
