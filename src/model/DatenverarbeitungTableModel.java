package model;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 * Benutzerdefiniertes Tabellenmodell, das das Hinzufügen und Löschen von
 * Zeilen sowie die Neuanordnung erlaubt.
 */
public final class DatenverarbeitungTableModel extends DefaultTableModel
        implements Reorderable {
  /**
   * Vertauscht zwei Reihen.
   * @param fromIndex Erste Reihe zum Tausch.
   * @param toIndex Zweite Reihe zum Tausch
   */
  public void reorder(final int fromIndex, final int toIndex) {
    Vector oldRow = this.dataVector.get(fromIndex);
    Vector newRow = this.dataVector.get(toIndex);
    this.dataVector.set(fromIndex, newRow);
    this.dataVector.set(toIndex, oldRow);
    this.fireTableDataChanged();
  }

  /**
   * Löscht die Spalte an einem bestimmten Index.
   * @param index Index der zu löschenden Spalte.
   */
  public void deleteColumnAt(final int index) {

    for (Vector row : this.dataVector) {
      row.removeElementAt(index);
    }

    this.columnIdentifiers.removeElementAt(index);
    this.fireTableStructureChanged();
    this.fireTableDataChanged();
  }

  /**
   * Fügt eine Zeile vor einem bestimmten Index hinzu.
   * @param row Zeile, wo die neuen Zeile soll sein.
   */
  public void addRowAt(final int row) {
    this.dataVector.add(row, new Vector(this.getColumnCount()));
    this.fireTableStructureChanged();
    this.fireTableDataChanged();
  }

  /**
   * Ruft Daten aus einer bestimmten Spalte ab.
   * @param index Spaltenindex, wo sich die Daten befinden.
   * @return Eine ArrayList mit den Daten aus der Spalte.
   */
  public ArrayList<Object> getColData(final int index) {
    ArrayList<Object> result = new ArrayList<>();

    for (Vector row : this.dataVector) {
      result.add(row.get(index));
    }

    return result;
  }

  /**
   * Konstruktor für ein Tabellenmodell mit einer bestimmten Anzahl von Zeilen
   * und Spalten.
   * @param rows Nummer der Zeilen.
   * @param cols Nummer der Spalten.
   */
  public DatenverarbeitungTableModel(final int rows, final int cols) {
    super(rows, cols);
  }
}
