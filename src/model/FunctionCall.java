package model;

import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;

/**
 * Definiert eine Funktionsaufruf.
 */
public final class FunctionCall {
    /**
     * Der Name der Funktion.
     */
    private String funcName = "";
    /**
     * Die Funktionsargumente.
     */
    private List<Object> funcArgs = emptyList();

    /**
     * @return Der Name des Funktions.
     */
    public String getFuncName() {
        return this.funcName;
    }

    /**
     * @param fn Der neuen Name der Funktion.
     */
    public void setFuncName(final String fn) {
        Objects.requireNonNull(fn, "Funktionname muss nicht Null sein");
        this.funcName = fn;
    }

    /**
     * @return Der Funktionsargumenten.
     */
    public List<Object> getFuncArgs() {
        return this.funcArgs;
    }

    /**
     * @param l Der neuen Funktionsargumenten.
     */
    public void setFuncArgs(final List<Object> l) {
        Objects.requireNonNull(l, "List muss nicht Null sein");
        this.funcArgs = l;
    }
}
