package model;

/**
 * Eine Interface, sodass Zeilen wiederordnet werden.
 */
public interface Reorderable {
  /**
   * @param var1 Zeile 1 für Wechseln.
   * @param var2 Zeile 2 für Wechseln.
   */
  void reorder(int var1, int var2);
}
