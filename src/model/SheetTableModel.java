// SheetTableModel.java

package model;

import java.util.Iterator;
import javax.swing.table.AbstractTableModel;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;


/**
 * Der Modell ƒür der Sheet, sodass es mit einer JTable funktionert.
 */
public final class SheetTableModel extends AbstractTableModel {
  /**
   * Der Sheet für der Table.
   */
  private final Sheet sheet;
  /**
   * Der FormulaEvaluator, sodass Formulae evaluieren können.
   */
  private final FormulaEvaluator eval;

  /**
   * @return Bekommt der Anzahl der Zeilen.
   */
  public int getRowCount() {
    return this.sheet.getLastRowNum() + 1;
  }

  /**
   * Bekommt der Anzahl der Spalten in der TableModel.
   * @return Der Anzahl der Spalten.
   */
  public int getColumnCount() {
    int maxCol = 0;
    Iterator<Row> ri = this.sheet.rowIterator();

    while (ri.hasNext()) {
      Row row = ri.next();
      if (row.getPhysicalNumberOfCells() > maxCol) {
        maxCol = row.getPhysicalNumberOfCells();
      }
    }

    return maxCol;
  }

  /**
   * Bekommt der Wert an einer besonderen Platz (Spalte/Zeile).
   * @param rowIndex Index der Zeile.
   * @param columnIndex Index der Spalte.
   * @return Der Wert an der besonderen Spalte/Zeile.
   */
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    if ((sheet.getRow(rowIndex) == null) || sheet.getRow(rowIndex).getCell(
            columnIndex) == null) {
      return "";
    } else {
      var cell = sheet.getRow(rowIndex).getCell(columnIndex);
      if (cell.getCellType() == CellType.BLANK) {
        return "";
      } else if (cell.getCellType() == CellType.BOOLEAN) {
        return cell.getBooleanCellValue();
      } else if (cell.getCellType() == CellType.ERROR) {
        return cell.getErrorCellValue();
      } else if (cell.getCellType() == CellType.FORMULA) {
        return eval.evaluate(cell).getNumberValue();
      } else if (cell.getCellType() == CellType.NUMERIC) {
        return cell.getNumericCellValue();
      } else if (cell.getCellType() == CellType.STRING) {
        return cell.getStringCellValue();
      } else if (cell.getCellType() == CellType._NONE) {
        return "";
      } else {
        return "";
      }
    }
  }

  /**
   * @return Der Sheet für dieser Modell.
   */
  public Sheet getSheet() {
    return this.sheet;
  }

  /**
   * @return Der FormulaEvaluator für der Sheet.
   */
  public FormulaEvaluator getEval() {
    return this.eval;
  }

  /**
   * Konstruktor für SheetTableModel.
   * @param s Excel-Sheet.
   * @param e FormulaEvaluator, sodass Formulas evaluieren können.
   */
  public SheetTableModel(final Sheet s, final FormulaEvaluator e) {
    super();
    this.sheet = s;
    this.eval = e;
  }
}
