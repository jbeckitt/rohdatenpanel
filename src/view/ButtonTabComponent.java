package view;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Der "X"-Button für jeder Tab.
 */
public final class ButtonTabComponent extends JPanel {

  /**
   * Der Große der Tab-Button.
   */
  private static final int TAB_BUTTON_SIZE = 17;
  /**
   * Der Große der "X" im Knopf.
   */
  private static final int X_DELTA = 6;
  /**
   * Offset für rechte Grenze.
   */
  private static final int RIGHT_BORDER = 5;
  /**
   * Der TabbedPane, wo die Knöpfe gehen sollen.
   */
  private final JTabbedPane pane;
  /**
   * Wenn True, der Datel soll gespeichert werden.
   */
  private final AtomicBoolean dirty;
  /**
   * Der Titel des Tabs.
   */
  private final String title;
  /**
   * Der MouseListener für der Tab, sodass der Knopf funktionert.
   */
  private static final MouseListener BUTTON_MOUSE_LISTENER =
          new MouseAdapter() {
    public void mouseEntered(final MouseEvent e) {
      Component component = e.getComponent();
      if (component instanceof AbstractButton) {
        ((AbstractButton) component).setBorderPainted(true);
      }

    }

    public void mouseExited(final MouseEvent e) {
      Component component = e.getComponent();
      if (component instanceof AbstractButton) {
        ((AbstractButton) component).setBorderPainted(false);
      }

    }
  };

  /**
   * @return Ob der Datei speichern soll.
   */
  public AtomicBoolean getDirty() {
    return this.dirty;
  }

  /**
   * @return Der Titel des Tabs
   */
  public String getTitle() {
    return this.title;
  }

  /**
   * @param jtp TabbedPane, wo der Knopf gehen soll.
   * @param d Dirty-Variable, um Wechseln zu folgen.
   * @param t Title des Tabs.
   */
  public ButtonTabComponent(final JTabbedPane jtp, final AtomicBoolean d,
                            final String t) {
    super(new FlowLayout(FlowLayout.LEFT, 0, 0));
    this.dirty = d;
    this.title = t;
    if (jtp == null) {
      throw new NullPointerException("TabbedPane is null");
    } else {
      this.pane = jtp;
      this.setOpaque(false);
      JLabel label = new JLabel() {
        public String getText() {
          int i = jtp.indexOfTabComponent(ButtonTabComponent.this);
          if (i != -1) {
            return jtp.getTitleAt(i);
          }
          return null;
        }
      };
      this.add(label);
      label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, RIGHT_BORDER));
      JButton button = new TabButton();
      this.add(button);
      this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    }
  }

  /**
   * Der Schließen-Knopf für jeder Tab.
   */
  private final class TabButton extends JButton implements ActionListener {
    /**
     * @param e Der ActionEvent, die wir bearbeiten.
     */
    public void actionPerformed(final ActionEvent e) {
      int result;
      if (!ButtonTabComponent.this.getDirty().get()) {
        result = ButtonTabComponent.this.pane.indexOfTabComponent(
                ButtonTabComponent.this);
        if (result != -1) {
          ButtonTabComponent.this.pane.remove(result);
        }
      } else {
        result = JOptionPane.showConfirmDialog(ButtonTabComponent.this.pane,
                String.format(
                        "Möchten Sie dieses neue Tabelle \"%s\" behalten?",
                        ButtonTabComponent.this.getTitle()), "",
                JOptionPane.YES_NO_CANCEL_OPTION);
        int i;
        if (result == 1) {
          i = ButtonTabComponent.this.pane.indexOfTabComponent(
                  ButtonTabComponent.this);
          if (i != -1) {
            ButtonTabComponent.this.pane.remove(i);
          }
        } else if (result == 0) {
          i = ButtonTabComponent.this.pane.indexOfTabComponent(
                  ButtonTabComponent.this);

          DatenverarbeitungTabellenPanel panel =
                  (DatenverarbeitungTabellenPanel)
                          ButtonTabComponent.this.pane.getComponentAt(i);
          try {
            panel.speichernButtonPressed();
          } catch (IOException ioException) {
            ioException.printStackTrace();
          }
        }
      }

    }

    /**
     * Wie man der UI aktualisiert.
     */
    public void updateUI() {
    }

    /**
     * @param g Der Graphics-Objekt, mit dem man die Component zeichnet.
     */
    protected void paintComponent(final Graphics g) {

      super.paintComponent(g);

      Graphics2D g2 = (Graphics2D) g.create();
      if (this.getModel().isPressed()) {
        g2.translate(1, 1);
      }

      g2.setStroke(new BasicStroke(2.0F));
      g2.setColor(Color.BLACK);

      if (this.getModel().isRollover()) {
        g2.setColor(Color.RED);
      }

      int delta = X_DELTA;
      g2.drawLine(delta, delta, this.getWidth() - delta - 1,
              this.getHeight() - delta - 1);
      g2.drawLine(this.getWidth() - delta - 1, delta, delta,
              this.getHeight() - delta - 1);
      g2.dispose();
    }

    /**
     * Konstruktor für TabButton.
     */
    TabButton() {
      int size = TAB_BUTTON_SIZE;
      this.setPreferredSize(new Dimension(size, size));
      this.setToolTipText("Diese Tab schließen");
      this.setUI(new BasicButtonUI());
      this.setContentAreaFilled(false);
      this.setFocusable(false);
      this.setBorder(BorderFactory.createEtchedBorder());
      this.setBorderPainted(false);
      this.addMouseListener(ButtonTabComponent.BUTTON_MOUSE_LISTENER);
      this.setRolloverEnabled(true);
      this.addActionListener(this);
    }
  }


}
