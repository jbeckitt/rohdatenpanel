package view;

import controller.ExcelReader;
import model.SheetTableModel;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Der Panel, wo der Excel-Daten angezeigt sollen.
 */
public final class DatenansichtTabellenPanel extends JPanel {
  /**
   * Der Excel-Reader, die die Excel-Dateien liest.
   */
  private final ExcelReader er;
  /**
   * Der TabbedPane für der verschiedene Sheets.
   */
  private JTabbedPane tabellenTabbedPane;

  /**
   * Initialisiert der Tabellen in der Excel-Datei.
   */
  private void initTabellen() {

    for (int i = 0; i < this.er.getSheetCount(); i++) {
      DatenansichtTable t = new DatenansichtTable();

      SheetTableModel sheetTableModel = new SheetTableModel(this.er.getSheet(i),
              this.er.getEvaluator());

      t.setModel(sheetTableModel);
      t.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
      t.setSelectionMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
      t.setCellSelectionEnabled(true);

      JScrollPane jsp = new JScrollPane();
      jsp.setViewportView(t);
      jsp.setRowHeaderView(t.buildRowHeader());

      this.tabellenTabbedPane.add(jsp, this.er.getSheet(i).getSheetName());
    }


  }

  /**
   * Bekommt der gewählte Zellen.
   * @return Der gewählte Zellen.
   */
  public ArrayList<ArrayList<Object>> getSelectedCells() {
    JScrollPane sp = (JScrollPane) this.tabellenTabbedPane.getComponentAt(
            tabellenTabbedPane.getSelectedIndex());
    DatenansichtTable dt = (DatenansichtTable) sp.getViewport().getView();
    ArrayList<ArrayList<Object>> res = new ArrayList<>();
    for (var col : dt.getSelectedColumns()) {
      var innerres = new ArrayList<Object>();
      for (var row : dt.getSelectedRows()) {
        innerres.add(dt.getValueAt(row, col));
      }
      res.add(innerres);
    }
    return res;
  }

  /**
   * Initialisert der Komponenten der Panel.
   */
  private void initComponents() {
    this.tabellenTabbedPane = new JTabbedPane();
    this.tabellenTabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
    this.setLayout(new BorderLayout());
    this.add(this.tabellenTabbedPane, "Center");
  }

  /**
   * Konstruktor für DatenansichtTabellenPanel.
   * @param filePath Path für Excel-Datei.
   * @throws IOException Wenn der Datei nicht gelesen wird.
   */
  public DatenansichtTabellenPanel(final String filePath) throws IOException {
    this.initComponents();
    this.er = new ExcelReader(filePath);

    this.initTabellen();
  }
}
