package view;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.AbstractListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;

/**
 * Unterklasse von JTable, die Zeilenüberschriften hinzufügt.
 */
public final class DatenansichtTable extends JTable {

  /**
   * Der Breite der Zeilenköpfe.
   */
  private static final int ROW_HEADER_WIDTH = 50;
  /**
   * Der Geringstwert der Große für Zeilen.
   */
  private static final int MIN_ROW_HEIGHT = 12;
  /**
   * Der Offset für Mausbewegung.
   */
  private static final int MOUSE_DELTA = 3;
  /**
   * Erstellt Zeilenüberschriften für jede Zeile, beginnend mit 1.
   * @return Liste der Zeilenüberschriften.
   */
  public JList<String> buildRowHeader() {
    ArrayList<String> headers = new ArrayList<>();
    for (int i = 1; i <= this.getRowCount(); i++) {
      headers.add(String.valueOf(i));
    }


    ListModel<String> lm = (new AbstractListModel<>() {
      public int getSize() {
        return headers.size();
      }

      public String getElementAt(final int index) {
        return headers.get(index);
      }
    });
    final JList<String> rowHeader = new JList<>(lm);
    rowHeader.setOpaque(false);
    int rowHeaderWidth = ROW_HEADER_WIDTH;
    rowHeader.setFixedCellWidth(rowHeaderWidth);
    MouseInputAdapter mouseAdapter = new MouseInputAdapter() {
      private Cursor oldCursor;
      private final Cursor resizeCursor;
      private int index;
      private int oldY;

      public void mousePressed(final MouseEvent e) {
        super.mousePressed(e);
      }

      public void mouseMoved(final MouseEvent e) {
        super.mouseMoved(e);
        int prevI = this.getLocationToIndex(new Point(e.getX(),
                e.getY() - MOUSE_DELTA));
        int nextI = this.getLocationToIndex(new Point(e.getX(),
                e.getY() + MOUSE_DELTA));
        if (prevI != -1 && prevI != nextI) {
          if (!this.isResizeCursor()) {
            this.oldCursor = rowHeader.getCursor();
            rowHeader.setCursor(this.resizeCursor);
            this.index = prevI;
          }
        } else if (this.isResizeCursor()) {
          rowHeader.setCursor(this.oldCursor);
        }

      }

      private int getLocationToIndex(final Point point) {
        int i = rowHeader.locationToIndex(point);
        if (!rowHeader.getCellBounds(i, i).contains(point)) {
          i = -1;
        }

        return i;
      }

      public void mouseReleased(final MouseEvent e) {
        super.mouseReleased(e);
        if (this.isResizeCursor()) {
          rowHeader.setCursor(this.oldCursor);
          this.index = -1;
          this.oldY = -1;
        }

      }

      public void mouseDragged(final MouseEvent e) {
        super.mouseDragged(e);
        if (this.isResizeCursor() && this.index != -1) {
          int y = e.getY();
          if (this.oldY != -1) {
            int inc = y - this.oldY;
            int oldRowHeight = DatenansichtTable.this.getRowHeight(this.index);
            if (oldRowHeight > MIN_ROW_HEIGHT || inc > 0) {
              int rowHeight = Math.max(MIN_ROW_HEIGHT, oldRowHeight + inc);
              DatenansichtTable.this.setRowHeight(this.index, rowHeight);
              if (rowHeader.getModel().getSize() > this.index + 1) {
                int rowHeight1 = DatenansichtTable.this.getRowHeight(
                        this.index + 1) - inc;
                rowHeight1 = Math.max(MIN_ROW_HEIGHT, rowHeight1);
                DatenansichtTable.this.setRowHeight(this.index + 1, rowHeight1);
              }
            }
          }

          this.oldY = y;
        }

      }

      private boolean isResizeCursor() {
        return rowHeader.getCursor() == this.resizeCursor;
      }

      {
        this.resizeCursor = Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
        this.index = -1;
        this.oldY = -1;
      }
    };
    rowHeader.addMouseListener(mouseAdapter);
    rowHeader.addMouseMotionListener(mouseAdapter);
    rowHeader.addMouseWheelListener(mouseAdapter);
    rowHeader.setCellRenderer(new RowHeaderRenderer(this));
    rowHeader.setBackground(this.getBackground());
    rowHeader.setForeground(this.getForeground());
    return rowHeader;
  }

  /**
   * Renderer, sodass Min,Max,Mittelwert angezeigt werden.
   */
  public static final class RowHeaderRenderer extends JLabel
          implements ListCellRenderer<Object> {
    /**
     * Die JTable, in der der Renderer arbeiten wird.
     */
    private final JTable table;

    /**
     * Ruft eine Renderer-Komponente ab, die jede Zeilenüberschrift rendert.
     * @param list Liste als Ziel für Zeilenköpfe.
     * @param value Wert des Zeilenkopfes.
     * @param index Zeilenindex für Zeilenkopf.
     * @param isSelected Bestimmt, ob eine Zeile ausgewählt ist oder nicht.
     * @param cellHasFocus Bestimmt, ob die Zelle im Fokus ist oder nicht.
     * @return Komponent für Rendern der Zelle.
     */
    public Component getListCellRendererComponent(final JList list,
                                                  final Object value,
                                                  final int index,
                                                  final boolean isSelected,
                                                  final boolean cellHasFocus) {
      String s = "";
      if (value != null) {
        s = value.toString();
      }
      this.setText(s);
      this.setPreferredSize(null);
      this.setPreferredSize(new Dimension(
              (int) this.getPreferredSize().getWidth(),
              this.table.getRowHeight(index)));
      list.firePropertyChange("cellRenderer", 0, 1);
      return this;
    }

    /**
     * Konstruktor für den Zeilenkopf-Renderer.
     * @param t Tabelle für Zeilenköpfe.
     */
    public RowHeaderRenderer(final JTable t) {
      this.table = t;
      JTableHeader header = this.table.getTableHeader();
      this.setOpaque(true);
      this.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
      this.setHorizontalAlignment(0);
      this.setForeground(header.getForeground());
      this.setBackground(header.getBackground());
      this.setFont(header.getFont());
      this.setDoubleBuffered(true);
    }
  }
}
