package view;

import controller.HeaderSelector;
import model.DatenverarbeitungTableModel;
import model.FunctionCall;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableModel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ItemEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Ein Panel, das es ermöglicht, Input/Output-Daten aus der Excel-Datei zu
 * bearbeiten.
 */
public final class DatenverarbeitungTabellenPanel extends JPanel {
  /**
   * Das Gewicht des Spalts zwischen den beiden Scheiben.
   */
  private static final double SPLIT_WEIGHT = 0.5D;
  /**
   * Die Input-Table.
   */
  private JTable inputTable = new JTable();
  /**
   * Die Output-Table.
   */
  private JTable outputTable = new JTable();
  /**
   * Der Modell für der Input-Table.
   */
  private DatenverarbeitungTableModel inputTableModel;
  /**
   * Der Modell für der Output-Table.
   */
  private DatenverarbeitungTableModel outputTableModel;
  /**
   * Der Split-Pane ƒür der zwei Tabellen.
   */
  private final JSplitPane split = new JSplitPane();
  /**
   * Dirty bit - dieses wird auf True gesetzt, wenn seit dem letzten
   * Speichern Änderungen vorgenommen wurden. Wird verwendet, um zu
   * verfolgen, wann der Benutzer/die Benutzerin zum Speichern aufgefordert
   * werden soll.
   */
  private final AtomicBoolean dirty = new AtomicBoolean(false);
  /**
   * Stack zur Verfolgung von Funktionsaufrufen, die rückgängig gemacht werden.
   * können.
   */
  private final Stack<FunctionCall> undoStack = new Stack<>();
  /**
   *
   */
  private final Stack<FunctionCall> redoStack = new Stack<>();
  /**
   * Der Toolbar für verschiedene Funktionen.
   */
  private final JToolBar bottomToolBar = new JToolBar();
  /**
   * Der Knopf für Datei-Speichern.
   */
  private final JButton speichernButton = new JButton();
  /**
   * Der Knopf für der Widerrufen der Änderungen.
   */
  private final JButton widerrufenButton = new JButton();
  /**
   * Der Knopf für der Wiederholen der Änderungen.
   */
  private final JButton wiederholenButton = new JButton();
  /**
   * Zeigt der Rechnungen oder nicht.
   */
  private final JCheckBox showCalcCheckBox = new JCheckBox();


  /**
   * @return Ob der Dirty-Bit True oder False ist.
   */
  public AtomicBoolean getDirty() {
    return this.dirty;
  }


  /**
   * Initialisiert alle Komponente.
   */
  private void initComponents() {
    this.split.setOrientation(1);
    this.setLayout(new BorderLayout());
    this.setInputTableModel(new DatenverarbeitungTableModel(0, 0));
    this.setOutputTableModel(new DatenverarbeitungTableModel(0, 0));

    this.inputTable = new JTable(this.getInputTableModel());


    this.inputTable.setSelectionMode(0);
    this.inputTable.setDropMode(DropMode.USE_SELECTION);
    this.inputTable.setDragEnabled(true);
    this.inputTable.setTransferHandler(new TableTransferHandler());
    this.inputTable.setFillsViewportHeight(true);

    this.inputTable.getTableHeader().setDefaultRenderer(
            new DatenverarbeitungTableHeader(false,
                    this.getInputTableModel()));

    HeaderSelector headerSelector = new HeaderSelector(this.inputTable,
            this.getInputTableModel(), this.outputTable,
            this.getOutputTableModel(), this);

    inputTable.getTableHeader().addMouseListener(headerSelector);

    this.inputTable.getModel().addTableModelListener(
            e -> DatenverarbeitungTabellenPanel.this.getDirty().set(true));

    this.outputTable = new JTable(this.getOutputTableModel());
    this.outputTable.setSelectionMode(0);
    this.outputTable.setDropMode(DropMode.USE_SELECTION);
    this.outputTable.setDragEnabled(true);
    this.outputTable.setTransferHandler(new TableTransferHandler());

    HeaderSelector hs = new HeaderSelector(this.inputTable,
            this.getInputTableModel(), this.outputTable,
            this.getOutputTableModel(), this);

    outputTable.getTableHeader().addMouseListener(hs);
    //outputTable.getTableHeader().setPreferredSize(new Dimension(0, 100));
    this.outputTable.setFillsViewportHeight(true);
    this.outputTable.getTableHeader().setDefaultRenderer(
            new DatenverarbeitungTableHeader(false,
                    this.getOutputTableModel()));
    JScrollPane inputScrollPane = new JScrollPane(this.inputTable);
    this.split.setLeftComponent(inputScrollPane);
    JScrollPane outputScrollPane = new JScrollPane(this.outputTable);
    this.split.setRightComponent(outputScrollPane);
    this.split.setResizeWeight(SPLIT_WEIGHT);
    this.add(this.split, "Center");
    this.widerrufenButton.setIcon(new ImageIcon(this.getClass().getResource(
            "/img/edit-undo-22.png")));
    this.widerrufenButton.addActionListener(it ->
            DatenverarbeitungTabellenPanel.this.widerrufenButtonPressed());
    this.widerrufenButton.setToolTipText("Widerrufen");
    this.widerrufenButton.setEnabled(false);
    this.wiederholenButton.setIcon(new ImageIcon(this.getClass().getResource(
            "/img/edit-redo-22.png")));
    this.wiederholenButton.setToolTipText("Wiederholen");
    this.wiederholenButton.addActionListener(it ->
            DatenverarbeitungTabellenPanel.this.wiederholenButtonPressed());
    this.wiederholenButton.setEnabled(false);
    this.speichernButton.setText("Datensatz speichern");
    this.speichernButton.addActionListener(it -> {
      try {
        DatenverarbeitungTabellenPanel.this.speichernButtonPressed();
      } catch (IOException e) {
        e.printStackTrace();
      }
    });

    this.showCalcCheckBox.setText("Berechnungen anzeigen");
    this.showCalcCheckBox.addItemListener(e -> {
      ((DatenverarbeitungTableHeader) (
              this.inputTable.getTableHeader().getDefaultRenderer()))
              .setShowCalcs(e.getStateChange() == ItemEvent.SELECTED);
      ((DatenverarbeitungTableHeader) (
              this.outputTable.getTableHeader().getDefaultRenderer()))
              .setShowCalcs(e.getStateChange() == ItemEvent.SELECTED);
      // Stellt sicher, dass die Größe der Kopfzeilen angepasst wird.
      split.setLeftComponent(new JScrollPane(inputTable));
      split.setRightComponent(new JScrollPane(outputTable));
      split.repaint();
    });
    this.bottomToolBar.add(this.speichernButton);
    this.bottomToolBar.add(this.widerrufenButton);
    this.bottomToolBar.add(this.wiederholenButton);
    this.bottomToolBar.add(this.showCalcCheckBox);
    this.add(this.bottomToolBar, "South");
  }

  /**
   * Was wird gemacht, als der Wiederholen-Knopf gedruckt wird.
   */
  private void wiederholenButtonPressed() {
    this.dirty.set(true);
    FunctionCall call = this.redoStack.pop();
    // Führen spezifische Aktionen für die drei Arten von Funktionsaufrufen
    // durch.
    if (call.getFuncName().equals("addData")) {
      this.undoStack.add(call);
      this.widerrufenButton.setEnabled(true);
      StringBuilder sb;
      String title;
      Object column;
      // Wenn der Call aus der Output-Table kommt.
      if ((Boolean) call.getFuncArgs().get(0)) {
        sb = (new StringBuilder()).append("Output ");

        title = sb.append((
                this.getOutputTableModel().getColumnCount() + 1)).toString();
        column = call.getFuncArgs().get(1);

        this.getOutputTableModel().addColumn(title, (Object[]) column);
      } else {
        sb = (new StringBuilder()).append("Input ");

        title = sb.append((
                this.getInputTableModel().getColumnCount() + 1)).toString();
        column = call.getFuncArgs().get(1);

        this.getInputTableModel().addColumn(title, (Object[]) column);
      }
    } else {
      int cellNum;
      if (call.getFuncName().equals("addCells")) {
        this.undoStack.add(call);
        this.widerrufenButton.setEnabled(true);
        if ((Boolean) call.getFuncArgs().get(0)) {
          cellNum = (int) call.getFuncArgs().get(1);
          this.getOutputTableModel().insertRow(cellNum,
                  new Vector());
        } else {
          cellNum = (int) call.getFuncArgs().get(1);
          this.getInputTableModel().insertRow(cellNum,
                  new Vector());
        }
      } else if (call.getFuncName().equals("deleteSelected")) {
        this.undoStack.add(call);
        this.widerrufenButton.setEnabled(true);
        // Wenn der Output-Table der Ziel ist
        if ((Boolean) call.getFuncArgs().get(0)) {
          cellNum = (int) call.getFuncArgs().get(1);

          this.getOutputTableModel().removeRow(cellNum);
        } else {
          cellNum = (int) call.getFuncArgs().get(1);

          this.getInputTableModel().removeRow(cellNum);
        }
      }
    }

    if (this.undoStack.empty()) {
      this.widerrufenButton.setEnabled(false);
    }

    if (this.redoStack.empty()) {
      this.wiederholenButton.setEnabled(false);
    }

  }

  /**
   * Was passiert, als der Widerrufen-Knopf gedruckt wird.
   */
  private void widerrufenButtonPressed() {
    this.dirty.set(true);
    FunctionCall call = this.undoStack.pop();
    this.wiederholenButton.setEnabled(true);
    if (call.getFuncName().equals("addData")) {
      this.redoStack.push(call);
      this.wiederholenButton.setEnabled(true);
      if ((Boolean) call.getFuncArgs().get(0)) {
        this.getOutputTableModel().deleteColumnAt(
                this.getOutputTableModel().getColumnCount() - 1);
      } else {
        this.getInputTableModel().deleteColumnAt(
                this.getInputTableModel().getColumnCount() - 1);
      }
    } else {
      int rowNum;
      if (call.getFuncName().equals("addCells")) {
        this.redoStack.push(call);
        this.wiederholenButton.setEnabled(true);
        if ((Boolean) call.getFuncArgs().get(0)) {
          rowNum = (int) call.getFuncArgs().get(1);

          this.getOutputTableModel().removeRow(rowNum - 1);
        } else {
          rowNum = (int) call.getFuncArgs().get(1);
          this.getInputTableModel().removeRow(rowNum - 1);
        }
      } else if (call.getFuncName().equals("deleteSelected")) {
        this.redoStack.push(call);
        this.wiederholenButton.setEnabled(true);
        Object data;
        Vector<Vector> dataVector;
        if ((Boolean) call.getFuncArgs().get(0)) {
          rowNum = (int) call.getFuncArgs().get(1);

          this.getOutputTableModel().addRowAt((Integer) rowNum);

          dataVector = this.getOutputTableModel().getDataVector();

          data = call.getFuncArgs().get(2);
          data = ((ArrayList<Object>) data).get(0);

          dataVector.set(rowNum, (Vector<Vector>) data);
        } else {

          rowNum = (int) call.getFuncArgs().get(1);

          this.getInputTableModel().addRowAt((Integer) rowNum);

          dataVector = this.getInputTableModel().getDataVector();
          rowNum = (int) call.getFuncArgs().get(1);
          data = call.getFuncArgs().get(2);
          data = ((ArrayList) data).get(0);
          dataVector.set(rowNum, (Vector) data);
        }
      }
    }

    if (this.undoStack.empty()) {
      this.widerrufenButton.setEnabled(false);
    }

  }

  /**
   * Was passiert, als der Speichern-Knopf gedruckt wird
   * (Dateispeichern-Prozess).
   * @throws IOException Als ein Fehler in Dateispeichern gäbe.
   */
  public void speichernButtonPressed() throws IOException {
    // Öffne der Dialog für Datei-Speichern.
    JFileChooser fc = new JFileChooser();
    int returnVal = fc.showSaveDialog(this);
    if (returnVal == 0) {
      this.dirty.set(false);

      JTabbedPane tabbedPane =
              (JTabbedPane) SwingUtilities.getAncestorOfClass(
                      JTabbedPane.class, this);
      int i = 0;
      for (int t = tabbedPane.getTabCount(); i < t; ++i) {
        if (SwingUtilities.isDescendingFrom((Component) this,
                tabbedPane.getComponentAt(i))) {
          File f = fc.getSelectedFile();
          tabbedPane.setTitleAt(i, f.getName());
          break;
        }
      }

      File f = fc.getSelectedFile();
      BufferedWriter writer = new BufferedWriter(new FileWriter(f));
      CSVPrinter csvPrinter = new CSVPrinter(writer,
              CSVFormat.TDF);

      int inputTableModelSize =
              this.getInputTableModel().getDataVector().size();

      int difference;
      if (inputTableModelSize
              > this.getOutputTableModel().getDataVector().size()) {
        inputTableModelSize = this.getInputTableModel().getDataVector().size();

        difference =
                inputTableModelSize
                        - this.getOutputTableModel().getDataVector().size();

        for (i = 0; i < difference; i++) {
          this.getOutputTableModel().addRow(new Vector<>());
        }
      } else if (inputTableModelSize
              < this.getOutputTableModel().getDataVector().size()) {
        inputTableModelSize = this.getInputTableModel().getDataVector().size();

        difference =
                inputTableModelSize
                        - this.getOutputTableModel().getDataVector().size();

        for (i = 0; i < difference; i++) {
          this.getInputTableModel().addRow(new Vector<>());
        }
      }


      ArrayList<ArrayList<Object>> zipped = new ArrayList<>();


      for (i = 0; i < this.getInputTableModel().getDataVector().size(); i++) {
        Vector<Object> merge = new Vector<Object>();

        // Ersetzt leere Zellen durch 0.0.
        for (int j = 0; j < getInputTableModel().getDataVector().get(i).size();
             j++) {
          if (getInputTableModel().getDataVector().get(i).get(j).equals("")) {
            getInputTableModel().getDataVector().get(i).set(j, 0.0f);
          }
        }
        for (int j = 0; j < getOutputTableModel().getDataVector().get(i).size();
             j++) {
          if (getOutputTableModel().getDataVector().get(i).get(j).equals("")) {
            getOutputTableModel().getDataVector().get(i).set(j, 0.0f);
          }
        }


        merge.addAll(getInputTableModel().getDataVector().get(i));
        merge.addAll(getOutputTableModel().getDataVector().get(i));
        zipped.add(new ArrayList<>(Arrays.asList(merge.toArray())));
      }

      csvPrinter.printRecords(zipped);
      csvPrinter.close();
    }

  }

  /**
   * Fügt Daten in der Tabellen als Spalten hinzu.
   * @param output Ob der Daten in der Output-Table oder Input-Table gehen soll.
   *               Wenn True geht es in der Output-Table.
   * @param dataArray Der Spalte der Daten, um hinzuzufügen.
   */
  public void addData(final boolean output, final Object[] dataArray) {
    this.dirty.set(true);
    FunctionCall call = new FunctionCall();
    call.setFuncName("addData");
    call.setFuncArgs(new ArrayList<Object>(Arrays.asList(output, dataArray)));
    this.undoStack.push(call);
    this.widerrufenButton.setEnabled(true);
    StringBuilder sb;
    for (int i = 0; i < dataArray.length; i++) {
      if (dataArray[i].equals("")) {
        dataArray[i] = 0.0f;
      }
    }
    if (output) {
      sb = (new StringBuilder()).append("Output ");

      this.getOutputTableModel().addColumn(sb.append(
              (this.getOutputTableModel().getColumnCount() + 1)).toString(),
              dataArray);
    } else {
      sb = (new StringBuilder()).append("Input ");

      this.getInputTableModel().addColumn(sb.append(
              (this.getInputTableModel().getColumnCount() + 1)).toString(),
              dataArray);
    }

  }

  /**
   * Löscht der ausgewahlten Zeile.
   */
  public void deleteSelected() {
    this.dirty.set(true);
    FunctionCall call = new FunctionCall();
    call.setFuncName("deleteSelected");
    boolean output = false;
    int[] selectedRows;
    List<Vector> rows = (new ArrayList<>());
    if (this.inputTable.getSelectedRowCount() > 0) {
      output = false;
      selectedRows = this.inputTable.getSelectedRows();
      for (int row : inputTable.getSelectedRows()) {
        rows.add(getInputTableModel().getDataVector().get(row));
        getInputTableModel().removeRow(row);
      }
    } else {
      output = true;
      selectedRows = this.outputTable.getSelectedRows();
      for (int row : outputTable.getSelectedRows()) {
        rows.add(getOutputTableModel().getDataVector().get(row));
        getOutputTableModel().removeRow(row);
      }
    }
    System.out.println(Arrays.toString(selectedRows));
    call.setFuncArgs(Arrays.asList(output, selectedRows[0], rows));
    this.undoStack.push(call);
    this.widerrufenButton.setEnabled(true);
  }

  /**
   * Fügt eine leere Zeile bei der Auswahl hinzu.
   */
  public void addCells() {
    this.dirty.set(true);
    FunctionCall call = new FunctionCall();
    call.setFuncName("addCells");
    boolean output = false;
    int row = 0;
    if (this.inputTable.getSelectedRowCount() > 0) {
      this.getInputTableModel().insertRow(this.inputTable.getSelectedRow(),
              new Vector());
      row = this.inputTable.getSelectedRow();
    }

    if (this.outputTable.getSelectedRowCount() > 0) {
      this.getOutputTableModel().insertRow(this.outputTable.getSelectedRow(),
              new Vector());
      row = this.outputTable.getSelectedRow();
    }

    call.setFuncArgs(Arrays.asList(output, row));
    this.undoStack.push(call);
    this.widerrufenButton.setEnabled(true);
  }

  /**
   * Einfacher Konstruktor für der TabellenPanel.
   */
  public DatenverarbeitungTabellenPanel() {
    this.initComponents();
  }

  /**
   * @return Der InputTableModel in dieser Ansicht.
   */
  public DatenverarbeitungTableModel getInputTableModel() {
    return inputTableModel;
  }

  /**
   * @param tableModel Der neue InputTableModel für der Input-Table.
   */
  public void setInputTableModel(
          final DatenverarbeitungTableModel tableModel) {
    this.inputTableModel = tableModel;
  }

  /**
   * @return Der OutputTableModel in dieser Ansicht.
   */
  public DatenverarbeitungTableModel getOutputTableModel() {
    return outputTableModel;
  }

  /**
   * @param tableModel Der neue OutputTableModel für der Output-Table.
   */
  public void setOutputTableModel(
          final DatenverarbeitungTableModel tableModel) {
    this.outputTableModel = tableModel;
  }

  /**
   * Erlaubt das Ziehen und Ablegen von Zeilen.
   */
  public abstract static class StringTransferHandler extends TransferHandler {
    /**
     * Exportiert eine String aus einer Komponente.
     * @param c Die Komponente Übertragungsbeginn.
     * @return Der exportierte String.
     */
    protected abstract String exportString(JComponent c);

    /**
     * @param c Die Zielkomponente der Übernahme.
     * @param str The string to export.
     */
    protected abstract void importString(JComponent c, String str);

    /**
     * Wenn ein Umzugsvorgang stattgefunden hat, muss das Quellobjekt aus dem
     * Orderbuch entfernt werden. Wenn ein Kopiervorgang stattgefunden hat,
     * muss nichts bereinigt werden.
     * @param c Der Beginnkomponent der Übertragung.
     * @param remove Soll man der exportierten String löschen?
     */
    protected abstract void cleanup(JComponent c, boolean remove);

    /**
     * @param c die Komponente, die die zu übertragenden Daten enthält; wird
     *          bereitgestellt, um die gemeinsame Nutzung von
     *          TransferHandlern zu ermöglichen.
     * @return die Darstellung der zu übertragenden Daten, oder null, wenn
     * die mit c verbundene Eigenschaft null ist
     */
    protected Transferable createTransferable(final JComponent c) {
      return new StringSelection(this.exportString(c));
    }

    /**
     * @param c Gibt den Typ der von der Quelle unterstützten
     *          Übertragungsaktionen zurück; jede bitweise OR Kombination
     *          von COPY, MOVE und LINK.
     *          Einige Modelle sind nicht veränderbar, so dass eine
     *          Übertragungsaktion von MOVE in diesem Fall nicht beworben werden
     *          sollte. Die Rückgabe von NONE deaktiviert Übertragungen von der
     *          Komponente.
     * @return COPY_OR_MOVE.
     */
    public int getSourceActions(final JComponent c) {
      return COPY_OR_MOVE;
    }

    /**
     * Veranlasst einen Transfer zu einer Komponente aus der Zwischenablage
     * oder einen DND-Drop-Vorgang. Das Transferable stellt die Daten dar,
     * die in die Komponente importiert werden sollen.
     * @param c die Komponente, die den Transfer erhalten soll; bereitgestellt,
     *          um die gemeinsame Nutzung von TransferHandlern zu ermöglichen
     * @param t Die Daten zu importieren.
     * @return True, wenn die Daten in die Komponente eingefügt wurden, sonst
     * False
     */
    public boolean importData(final JComponent c, final Transferable t) {
      if (this.canImport(c, t.getTransferDataFlavors())) {
        try {
          String str = (String) t.getTransferData(DataFlavor.stringFlavor);
          this.importString(c, str);
          return true;
        } catch (UnsupportedFlavorException | IOException ignored) {
        }
      }

      return false;
    }

    /**
     * Wird aufgerufen, nachdem Daten exportiert wurden. Diese Methode sollte
     * die Daten entfernen, die übertragen wurden, wenn die Aktion MOVE war.
     * @param c die Komponente, die die Quelle der Daten war.
     * @param data die übertragenen Daten.
     * @param action die tatsächliche Aktion, die durchgeführt wurde.
     */
    protected void exportDone(final JComponent c, final Transferable data,
                              final int action) {
      this.cleanup(c, action == 2);
    }

    /**
     * Gibt an, ob eine Komponente einen Import des gegebenen Datensatzes von
     * Datenaromen akzeptiert, bevor sie tatsächlich versucht, ihn zu
     * importieren.
     * @param c the component to receive the transfer; provided to enable
     *          sharing of TransferHandlers
     * @param flavors die verfügbaren Datenformate
     * @return True, wenn die Daten in die Komponente eingefügt werden
     * können, sonst False.
     */
    public boolean canImport(final JComponent c, final DataFlavor[] flavors) {

      for (DataFlavor flavor : flavors) {
        if (DataFlavor.stringFlavor.equals(flavor)) {
          return true;
        }
      }

      return false;
    }
  }

  /**
   * Erlaubt der Wiederordnung der Zeilen.
   */
  public static class TableTransferHandler extends StringTransferHandler {
    /**
     * Der Zeilen für Übertragung.
     */
    private int[] rows = null;
    /**
     * Ort, an dem Artikel hinzugefügt werden.
     */
    private int addIndex = -1; //Location where items were added
    /**
     * Nummer der hinzugefügte Items.
     */
    private int addCount = 0;  //Number of items added.

    /**
     * Exportiert eine String aus einer Komponente.
     * @param c Die Komponente Übertragungsbeginn.
     * @return Der exportierte String.
     */
    protected String exportString(final JComponent c) {
      JTable table = (JTable) c;
      rows = table.getSelectedRows();
      int colCount = table.getColumnCount();

      StringBuilder buff = new StringBuilder();

      for (int i = 0; i < rows.length; i++) {
        for (int j = 0; j < colCount; j++) {
          Object val = table.getValueAt(rows[i], j);
          if (val != null) {
            buff.append(val.toString());
          }
          if (j != colCount - 1) {
            buff.append(",");
          }
        }
        if (i != rows.length - 1) {
          buff.append("\n");
        }
      }

      return buff.toString();
    }

    /**
     * Importiert eine String.
     * @param c   Die Zielkomponente der Übernahme.
     * @param str The string to export.
     */
    protected void importString(final JComponent c, final String str) {
      JTable target = (JTable) c;
      DefaultTableModel model = (DefaultTableModel) target.getModel();
      int index = target.getSelectedRow();

      //Prevent the user from dropping data back on itself.
      //For example, if the user is moving rows #4,#5,#6 and #7 and
      //attempts to insert the rows after row #5, this would
      //be problematic when removing the original rows.
      //So this is not allowed.
      if (rows != null && index >= rows[0] - 1
              && index <= rows[rows.length - 1]) {
        rows = null;
        return;
      }

      int max = model.getRowCount();
      if (index < 0) {
        index = max;
      } else {
        index++;
        if (index > max) {
          index = max;
        }
      }
      addIndex = index;
      String[] values = str.split("\n");
      addCount = values.length;
      int colCount = target.getColumnCount();
      for (int i = 0; i < values.length && i < colCount; i++) {
        model.insertRow(index++, values[i].split(","));
      }
    }

    /**
     * @param c      Der Beginnkomponent der Übertragung.
     * @param remove Soll man der exportierten String löschen?
     */
    protected void cleanup(final JComponent c, final boolean remove) {
      JTable source = (JTable) c;
      if (remove && rows != null) {
        DefaultTableModel model =
                (DefaultTableModel) source.getModel();

        //If we are moving items around in the same table, we
        //need to adjust the rows accordingly, since those
        //after the insertion point have moved.
        if (addCount > 0) {
          for (int i = 0; i < rows.length; i++) {
            if (rows[i] > addIndex) {
              rows[i] += addCount;
            }
          }
        }
        for (int i = rows.length - 1; i >= 0; i--) {
          model.removeRow(rows[i]);
        }
      }
      rows = null;
      addCount = 0;
      addIndex = -1;
    }
  }


}
