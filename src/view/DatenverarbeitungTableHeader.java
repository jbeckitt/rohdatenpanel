package view;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import model.DatenverarbeitungTableModel;

/**
 * Tabellenüberschriften für die Ansicht Datenverarbeitung. Ermöglicht die
 * Anzeige von Minimum, Maximum und Durchschnitt.
 */
public final class DatenverarbeitungTableHeader extends JLabel
        implements TableCellRenderer {
  /**
   * Wenn True, zeigt Minimum, Maximum und Durchschnitt.
   */
  private boolean showCalcs;
  /**
   * DatenverarbeitungTableModel für der Headers.
   */
  private final DatenverarbeitungTableModel model;

  /**
   * Bekommt der neue Header mit der Minimum, Maxmium und Mittelwert.
   * @param table Die Tabelle für der Header.
   * @param value Der Name der Zelle, die gerendert werden soll.
   * @param isSelected True, wenn die Zelle mit hervorgehobener Auswahl
   *                   gerendert werden soll; andernfalls False.
   * @param hasFocus wenn zutreffend, Zelle entsprechend rendern.  Wenn die
   *                 Zelle bearbeitet werden kann, rendern Sie sie in der Farbe,
   *                 die für die Anzeige der Bearbeitung verwendet wird.
   * @param row den Zeilenindex der gezeichneten Zelle.
   * @param column den Spaltenindex der gezeichneten Zelle.
   * @return die zum Zeichnen der Zelle verwendete Komponente.
   */
  public Component getTableCellRendererComponent(final JTable table,
                                                 final Object value,
                                                 final boolean isSelected,
                                                 final boolean hasFocus,
                                                 final int row,
                                                 final int column) {
    this.setBackground(table.getTableHeader().getBackground());
    this.setForeground(table.getTableHeader().getForeground());
    if (this.showCalcs) {
      List<Float> calcs = this.calculate(column);
      String format = String.format("<html>%s<br/><b>Minimum: </b>%f<br>"
              + "<b>Maximum: </b>%f<br><b>Mittelwert:  </b>%f</html>",
              value, calcs.get(0), calcs.get(1), calcs.get(2));
      this.setText(format);
    } else {
      this.setText(String.valueOf(value));
    }

    return this;
  }

  /**
   * Rechnet der Minimum, Maximum und Mittelwert.
   * @param column Der Spalte für der Rechnungen.
   * @return Eine List der Minimum, Maximum und Mittelwert.
   */
  public List<Float> calculate(final int column) {
    List<Float> dataValues = new ArrayList<Float>();

    for (Object row : this.model.getColData(column)) {
      float f;
      try {
        f = Float.parseFloat(String.valueOf(row));
        dataValues.add(f);
      } catch (NumberFormatException nfe) {
        // Nicht zu den Daten für die Durchschnittswerte hinzufügen.
      } catch (NullPointerException ignored) {
      }
    }
    OptionalDouble average = dataValues.stream().mapToDouble(a -> a).average();
    ArrayList<Float> res = new ArrayList<>();
    if (dataValues.size() > 0) {
      res.add(Collections.min(dataValues));
    } else {
      res.add(0.0F);
    }
    if (dataValues.size() > 0) {
      res.add(Collections.max(dataValues));
    } else {
      res.add(0.0F);
    }
    if (average.isPresent()) {
      res.add((float) average.getAsDouble());
    } else {
      res.add((float) 0.0);
    }
    return res;
  }

  /**
   * Schaltet die Anzeige von Berechnungen ein oder aus.
   * @param sc Boolean umschalten - wahr = ein, falsch = aus.
   */
  public void setShowCalcs(final boolean sc) {
    this.showCalcs = sc;
  }


  /**
   * @param sc Ob Berechnungen ein oder aus sind.
   * @param m Der Modell zu benutzen.
   */
  public DatenverarbeitungTableHeader(final boolean sc,
                                      final DatenverarbeitungTableModel m) {
    super();
    this.showCalcs = sc;
    this.model = m;
  }
}
