package view;

import model.DatenverarbeitungTableModel;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;

/**
 * HeaderEditor, sodass man Spalten zwischen Input und Output wechseln kann.
 */
public final class HeaderEditor {
    /**
     * Hier, "Input" oder "Output".
     */
    private String[] items;
    /**
     * Der Input-Table.
     */
    private final JTable inputTable;
    /**
     * Der Modell für der Input-Table.
     */
    private final DatenverarbeitungTableModel inputTableModel;
    /**
     * Der Output-Table.
     */
    private final JTable outputTable;
    /**
     * Der Modell für der Output-Table.
     */
    private final DatenverarbeitungTableModel outputTableModel;
    /**
     * Der Panel, wo die Tabellen sind.
     */
    private final DatenverarbeitungTabellenPanel panel;

    /**
     * @return Die Items (hier Input und Output)
     */
    public String[] getItems() {
        return this.items;
    }

    /**
     * @param it Der neue Items für HeaderEditor.
     */
    public void setItems(final String[] it) {
        this.items = it;
    }

    /**
     * Zeigt der Editor, als der Header geclickt wird.
     * @param parent Der Parent-Component der Panels.
     * @param col Der Spalte für Editing.
     * @param currentValue Der CurrentValue der Input/Output.
     */
    public void showEditor(final Component parent, final int col,
                           final String currentValue) {
        String message = "Input or output? " + (col + 1) + ":";
        Object dialogResult;
        if (currentValue.startsWith("Input")) {
            dialogResult = JOptionPane.showInputDialog(parent, message,
                    "Input/Output", JOptionPane.INFORMATION_MESSAGE, null,
                    this.items, this.items[0]);
        } else {
            dialogResult = JOptionPane.showInputDialog(parent, message,
                    "Input/Output", JOptionPane.INFORMATION_MESSAGE, null,
                    this.items, this.items[1]);
        }
        if (dialogResult != null) {
            String retVal = (String) dialogResult;
            ArrayList<Object> copy;
            Collection<Object> copyArray;
            String newColName;
            if (currentValue.startsWith("Input") && retVal.equals("Output")) {
                copy = this.inputTableModel.getColData(col);
                this.inputTableModel.deleteColumnAt(col);
                newColName = "Output " + (
                        this.outputTableModel.getColumnCount() + 1);
                copyArray = copy;
                this.outputTableModel.addColumn(
                        newColName, copyArray.toArray(new Object[0]));
                this.outputTable.setModel(this.outputTableModel);
                this.outputTable.createDefaultColumnsFromModel();
            } else if (currentValue.startsWith("Output")
                    && retVal.equals("Input")) {
                copy = this.outputTableModel.getColData(col);
                this.outputTableModel.deleteColumnAt(col);
                newColName = "Input " + (
                        this.inputTableModel.getColumnCount() + 1);
                copyArray = copy;
                this.inputTableModel.addColumn(newColName,
                        copyArray.toArray(new Object[0]));
                this.inputTable.setModel(this.inputTableModel);
                this.inputTable.createDefaultColumnsFromModel();
            }
        }
    }


    /**
     * @param it Input-Table.
     * @param itm Model für Input-Table.
     * @param ot Output-Table
     * @param otm Model für Output-Table
     * @param p Panel, wo die Tabellen gehen sollen.
     */
    public HeaderEditor(final JTable it, final DatenverarbeitungTableModel itm,
                        final JTable ot, final DatenverarbeitungTableModel otm,
                        final DatenverarbeitungTabellenPanel p) {
        super();
        this.inputTable = it;
        this.inputTableModel = itm;
        this.outputTable = ot;
        this.outputTableModel = otm;
        this.panel = p;
        this.items = new String[]{"Input", "Output"};
    }
}
