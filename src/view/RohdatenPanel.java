package view;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileFilter;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Der RohdatenPanel - der Panel, wo man Dateien importieren und exportieren
 * kann!
 */
class RohdatenPanel extends JPanel {
  /**
   * Der Resize Weight für der OuterPane.
   */
  public static final double OUTER_RESIZE_WEIGHT = 0.9;
  /**
   * Der Resize Weight für der InnerPane.
   */
  public static final double INNER_RESIZE_WEIGHT = 0.5;
  /**
   * Der TabbedPane für Datenansicht-Panels.
   */
  private JTabbedPane datenansichtTabbedPane = null;
  /**
   * Der TabbedPane für Datenverarbeitung-Panels.
   */
  private JTabbedPane datenverarbeitungTabbedPane = null;
  /**
   * Der neue Panel, wo die DateiverarbeitungTabbedPane gehen soll.
   */
  private final JPanel datenverarbeitungPanel = new JPanel();

  /**
   * Initialisiert alle Komponente.
   */
  private void initComponents() {
    final JSplitPane outerSplitPane = new JSplitPane();
    final JSplitPane innerSplitPane = new JSplitPane();
    final JPanel datenansichtPanel = new JPanel();
    this.datenansichtTabbedPane = new JTabbedPane();
    final JLabel datenverarbeitungLabel = new JLabel();
    this.datenverarbeitungTabbedPane = new JTabbedPane();
    final JToolBar datenansichtToolBar = new JToolBar();
    final JButton excelOffnenButton = new JButton();
    final JButton ubertragenButton = new JButton();
    final JPanel platzhalter = new JPanel();
    final JToolBar datenverarbeitungToolBar = new JToolBar();
    final JButton neuButton = new JButton();
    final JButton cellHinzufuegenButton = new JButton();
    final JButton cellLoeschenButton = new JButton();
    this.setLayout(new BorderLayout());
    outerSplitPane.setResizeWeight(OUTER_RESIZE_WEIGHT);
    innerSplitPane.setResizeWeight(INNER_RESIZE_WEIGHT);
    outerSplitPane.setOrientation(0);
    datenansichtPanel.setLayout(new BorderLayout());
    datenansichtPanel.add(datenansichtToolBar, "North");
    datenansichtToolBar.setFloatable(false);
    datenansichtToolBar.add(new JLabel("Datenansicht"));
    datenansichtToolBar.add(new JToolBar.Separator());
    excelOffnenButton.setText("Datei öffnen");
    excelOffnenButton.setIcon(new ImageIcon(
            this.getClass().getResource("/img/document-open-16.png")));
    excelOffnenButton.addActionListener(new DatenansichtDateiOffnenHandler());
    datenansichtToolBar.add(excelOffnenButton);
    ubertragenButton.setText("Gewählte Daten übertragen");
    ubertragenButton.setIcon(new ImageIcon(
            this.getClass().getResource("/img/go-next-skip-16.png")));
    ubertragenButton.addActionListener(new DatenubertragungHandler());
    datenansichtToolBar.add(ubertragenButton);
    neuButton.setText("Neu");
    neuButton.setIcon(new ImageIcon(
            this.getClass().getResource("/img/document-new-16.png")));
    neuButton.addActionListener(new NeuHandler());
    final JTabbedPane dtp = this.datenansichtTabbedPane;
    datenansichtPanel.add(dtp, "Center");
    innerSplitPane.setLeftComponent(datenansichtPanel);
    datenverarbeitungLabel.setText("Datenverarbeitung");
    this.datenverarbeitungPanel.setLayout(new BorderLayout());
    this.datenverarbeitungPanel.add(datenverarbeitungToolBar, "North");
    datenverarbeitungToolBar.add(new JLabel("Datenverarbeitung"));
    datenverarbeitungToolBar.addSeparator();
    datenverarbeitungToolBar.add(neuButton);
    datenverarbeitungToolBar.setFloatable(false);
    datenverarbeitungToolBar.addSeparator();
    cellHinzufuegenButton.setIcon(
            new ImageIcon(this.getClass().getResource(
                    "/img/edit-table-insert-row-below-16.png")));
    cellHinzufuegenButton.setToolTipText("Zelle hinzuf\u00fcgen");
    cellHinzufuegenButton.addActionListener(new ZelleHinzufuegenHandler());
    datenverarbeitungToolBar.add(cellHinzufuegenButton);
    cellLoeschenButton.setIcon(new ImageIcon(this.getClass().getResource(
            "/img/delete-table-row-16.png")));
    cellLoeschenButton.setToolTipText("Zelle löschen");
    cellLoeschenButton.addActionListener(new ZelleLoeschenHandler());
    datenverarbeitungToolBar.add(cellLoeschenButton);
    this.datenverarbeitungPanel.add(this.datenverarbeitungTabbedPane);
    innerSplitPane.setRightComponent(this.datenverarbeitungPanel);
    outerSplitPane.setTopComponent(innerSplitPane);
    final GroupLayout platzhalterLayout = new GroupLayout(platzhalter);
    outerSplitPane.setRightComponent(platzhalter);
    this.add(outerSplitPane, "Center");
  }

  /**
   * Initialisert der Panel - Initialisiert alle Komponente.
   */
  RohdatenPanel() {
    initComponents();
    // Erstellen eine neue leere Datei.
    final DatenverarbeitungTabellenPanel panel =
            new DatenverarbeitungTabellenPanel();
    final String title = "Unbenannt"
            + (RohdatenPanel.this.datenverarbeitungTabbedPane.getTabCount()
            + 1);
    RohdatenPanel.this.datenverarbeitungTabbedPane.addTab(title, panel);
    RohdatenPanel.this.datenverarbeitungTabbedPane.setTabComponentAt(
            RohdatenPanel.this.datenverarbeitungTabbedPane.getTabCount()
                    - 1, new ButtonTabComponent(
                    RohdatenPanel.this.datenverarbeitungTabbedPane,
                    panel.getDirty(), title));
  }

  /**
   * Was passiert, als der Datenübertragung-Knopf geklickt wird: Uberträgt Daten
   * zwischen der zwei Panels.
   */
  private class DatenubertragungHandler implements ActionListener {

    @Override
    public void actionPerformed(final ActionEvent e) {
      int datenansichtTabIndex =
              RohdatenPanel.this.datenansichtTabbedPane.getSelectedIndex();
      int datenverarbeitungTabIndex =
              RohdatenPanel.this.datenverarbeitungTabbedPane.getSelectedIndex();

      if (datenansichtTabIndex > -1) {
        DatenansichtTabellenPanel dtp = (DatenansichtTabellenPanel)
                datenansichtTabbedPane.getComponentAt(datenansichtTabIndex);
        var result = JOptionPane.showOptionDialog(getParent(),
        "Daten für Input oder Output?", "Daten übertragen",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                new String[]{"Input", "Output"}, "Input");
        if (result == JOptionPane.NO_OPTION) {
          if (datenverarbeitungTabIndex > -1) {
            DatenverarbeitungTabellenPanel dvtp =
                    (DatenverarbeitungTabellenPanel)
                            RohdatenPanel
                                    .this
                                    .datenverarbeitungTabbedPane.getComponentAt(
                                    datenverarbeitungTabIndex);
            for (ArrayList<Object> al : dtp.getSelectedCells()) {
              dvtp.addData(true, al.toArray());
            }
          }

        } else if (result == JOptionPane.YES_OPTION) {
          if (datenverarbeitungTabIndex > -1) {
            DatenverarbeitungTabellenPanel dvtp =
                    (DatenverarbeitungTabellenPanel)
                            RohdatenPanel.this.datenverarbeitungTabbedPane
                                    .getComponentAt(
                                    datenverarbeitungTabIndex);
            for (ArrayList<Object> al : dtp.getSelectedCells()) {
              dvtp.addData(false, al.toArray());
            }
          }

        }

      }
    }
  }

  /**
   * Was passiert, als der "Neu"-Knopf geklickt wird.
   */
  private final class NeuHandler implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent e) {
      final DatenverarbeitungTabellenPanel panel =
              new DatenverarbeitungTabellenPanel();
      final String title = "Unbenannt"
              + (RohdatenPanel.this.datenverarbeitungTabbedPane.getTabCount()
              + 1);
      RohdatenPanel.this.datenverarbeitungTabbedPane.addTab(title, panel);
      RohdatenPanel.this.datenverarbeitungTabbedPane.setTabComponentAt(
              RohdatenPanel.this.datenverarbeitungTabbedPane.getTabCount()
                      - 1, new ButtonTabComponent(
                              RohdatenPanel.this.datenverarbeitungTabbedPane,
                      panel.getDirty(), title));
    }

  }

  /**
   * Was passiert, als der Öffnen-Knopf geklickt wird.
   */
  private final class DatenansichtDateiOffnenHandler implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent e) {
      final JFileChooser jfc = new JFileChooser();
      jfc.setFileFilter(new ExcelFileFilter());
      final int result = jfc.showOpenDialog(RohdatenPanel.this.getParent());
      if (result == 0) {
        final File selectedFile;
        selectedFile = jfc.getSelectedFile();
        final DatenansichtTabellenPanel panel;
        try {
          panel = new DatenansichtTabellenPanel(selectedFile.getAbsolutePath());
          RohdatenPanel.this.datenansichtTabbedPane.addTab(
                  selectedFile.getName(), panel);
        } catch (IOException ioException) {
          ioException.printStackTrace();
        }


        RohdatenPanel.this.datenansichtTabbedPane.setTabComponentAt(
                RohdatenPanel.this.datenansichtTabbedPane.getTabCount()
                        - 1, new ButtonTabComponent(
                                RohdatenPanel.this.datenansichtTabbedPane,
                        new AtomicBoolean(false), ""));
      }
    }

  }

  /**
   * FileFilter, sodass nur Excel-Dateien gewählt werden können.
   */
  public static final class ExcelFileFilter extends FileFilter {
    @Override
    public boolean accept(final File f) {
      boolean b;
      if (f.isDirectory()) {
        b = true;
      } else {
        final String lowerCase = f.getName().toLowerCase();
        if (!lowerCase.endsWith(".xls")) {
          if (!lowerCase.endsWith(".xlsx")) {
            b = false;
            return b;
          }
        }
        b = true;
      }
      return b;
    }

    @Override
    public String getDescription() {
      return "Excel-Dateien (.xls/.xlsx)";
    }
  }

  /**
   * Fügt Zellen hinzu.
   */
  private final class ZelleHinzufuegenHandler implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent e) {
      final Component component = RohdatenPanel.this.datenverarbeitungTabbedPane
              .getComponentAt(
                      RohdatenPanel.this.datenverarbeitungTabbedPane
                              .getSelectedIndex());
      final DatenverarbeitungTabellenPanel panel =
              (DatenverarbeitungTabellenPanel) component;
      panel.addCells();
    }

  }

  /**
   * Löscht gewählte Zellen.
   */
  private final class ZelleLoeschenHandler implements ActionListener {
    @Override
    public void actionPerformed(final ActionEvent e) {
      final Component component = RohdatenPanel.this.datenverarbeitungTabbedPane
              .getComponentAt(RohdatenPanel.this.datenverarbeitungTabbedPane
                      .getSelectedIndex());
      final DatenverarbeitungTabellenPanel panel =
              (DatenverarbeitungTabellenPanel) component;
      panel.deleteSelected();
    }
  }


}
