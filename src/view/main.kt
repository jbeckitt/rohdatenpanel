
package view

import com.formdev.flatlaf.FlatLightLaf
import javax.swing.JFrame
import javax.swing.UIManager
import javax.swing.plaf.basic.BasicLookAndFeel
import javax.swing.plaf.metal.MetalLookAndFeel

fun main(args: Array<String>) {
    // Einfache Test-Funktion.
    // Die normale Swing LAF für macOS ist hässlich für Dunkelmodus.
    if (System.getProperty("os.name", "").startsWith("Mac OS")) {
        UIManager.setLookAndFeel("org.violetlib.aqua.AquaLookAndFeel");
    }
    val frame = JFrame()
    frame.contentPane.add(RohdatenPanel())
    frame.isVisible = true
}